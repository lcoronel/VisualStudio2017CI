﻿using System;

public class Perro
{
    //Atributos

    public string nombre;

    public int edad;

    public int energia;

    public int peso;


    //Constructor/es

    public Perro(string name)
	{
        nombre = name;
        edad = 1;
        energia = 100;
        peso = 10;
	}

    //Setter

    public void SetEdad(int edad)
    {
        edad = edad;
    }

    //Metodos
    public void aumentarEnergia(int value)
    {
        energia = energia + value;
        //energia += value;
    }

    public void cumplirAnios()
    {
        edad = edad + 1;
        //edad += 1;
    }


}
